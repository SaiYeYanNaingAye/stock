# How to run

### Technologies Usages
1. java
2. spring boot
3. h2 database
4. maven
5. freemaker


### Prerequisition

1. install jdk 11 https://openjdk.java.net/projects/jdk/11/
2. install maven https://maven.apache.org/download.cgi


To able to run app, you will need to build it

### Build project with Maven
Run below command    
    maven package
 or
	mvn install

After running command, you will get the jar file under projectfolder/target	
	
### Run Spring Boot app with java -jar command
    java -jar target/stock-0.0.1-SNAPSHOT.jar
    
### Run Spring Boot app using Maven
    mvn spring-boot:run
    
### Accessing application

1. Open the browser
2. type http://localhost:8080

    
