package com.stock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.stock.model.Stock;

@Repository
public interface StockRepository extends CrudRepository<Stock, Long> {
	@Query("select s from Stock s where s.symbol like %?1")
	List<Stock> findBySymbolEndsWith(String chars);
}
