package com.stock.service;

import java.util.List;

import com.stock.model.Stock;

public interface IStockService {
	List<Stock> findBySymbolEndsWith(String symbol);
}
