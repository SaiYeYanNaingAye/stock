package com.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stock.model.Stock;
import com.stock.repository.StockRepository;

@Service
public class StockService implements IStockService {

	@Autowired
	private StockRepository repository;

	@Override
	public List<Stock> findBySymbolEndsWith(String symbol) {
		var stocks = (List<Stock>) repository.findBySymbolEndsWith(symbol);
		return stocks;
	}

}
