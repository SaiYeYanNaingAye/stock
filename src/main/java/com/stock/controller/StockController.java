package com.stock.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.stock.model.Stock;
import com.stock.service.IStockService;
import com.stock.wswrapper.StockWrapper;

@Controller
public class StockController {
	@Autowired
	IStockService stockService;

	@GetMapping("/showStocksEnding")
	public String findStockSymbolEndsWith(Model model, @RequestParam String symbol) {

		var stocks = (List<Stock>) stockService.findBySymbolEndsWith(symbol);

		List<StockWrapper> stockList = new ArrayList<>();

		if (!stocks.isEmpty()) {
			for (int i = 0; i < stocks.size(); i++) {
				StockWrapper stockwp = new StockWrapper();
				stockwp.setSymbol(stocks.get(i).getSymbol());
				double price = (stocks.get(i).getBid() + stocks.get(i).getAsk()) / 2;
				stockwp.setPrice(price);
				stockwp.setTrend(stocks.get(i).getTrend());
				stockwp.setEvent(stocks.get(i).getEvent());
				stockList.add(stockwp);
			}
		}

		model.addAttribute("stocks", stockList);
		return "showStocks";
	}
}
